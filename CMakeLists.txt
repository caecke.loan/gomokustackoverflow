cmake_minimum_required( VERSION 3.0 )
project( projet_gomoku )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )

add_subdirectory( wt )
include_directories( ${WT_SOURCE_DIR}/src )
include_directories( ${WT_BINARY_DIR} )
link_directories( ${WT_BINARY_DIR}/src )

# programme principal
add_executable( main.out
        src/Couleur.hpp
        src/Point.cpp
        src/view/AppGomoku.cpp src/view/AppGomoku.hpp src/view/GridWidget.cpp src/view/GridWidget.hpp src/view/CaseWidget.cpp src/view/CaseWidget.hpp)
target_link_libraries( main.out wt wthttp wtdbo wtdbosqlite3)
add_dependencies( main.out wt wthttp wtdbo wtdbosqlite3 )
install( TARGETS main.out DESTINATION bin )

# programme de test
 add_executable( main_test.out
         src/tests/main_test.cpp
         src/Couleur.hpp
         src/Point.cpp
         src/tests/jeu_test.cpp)

 target_link_libraries( main_test.out ${PKG_CPPUTEST_LIBRARIES} )

