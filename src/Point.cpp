//
// Created by Loan Caecke on 2019-05-01.
//

#include "Point.hpp"
#include "Couleur.hpp"


Point::Point(int x, int y, Couleur couleur){
    _x = x;
    _y = y;
    _couleur = couleur;
}

Point::Point() {
    _x = 0;
    _y = 0;
    _couleur = Couleur::Vide;
}

Point::Point(int x, int y) {
    _x = x;
    _y = y;
    _couleur = Couleur::Vide;
}

void Point::setX(int _x) {
    Point::_x = _x;
}

void Point::setY(int _y) {
    Point::_y = _y;
}
