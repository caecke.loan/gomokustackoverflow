//
// Created by Loan Caecke on 2019-04-29.
//

#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, char ** argv) {
    return CommandLineTestRunner::RunAllTests(argc, argv);
}

