//
// Created by Loan Caecke on 2019-05-03.
//


#include "../Jeu.hpp"
#include "../exceptions/NoMorePawnLeft.hpp"
#include "../exceptions/WrongPawnCoordinates.hpp"

#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupJeu) { };

TEST(GroupJeu, Jeu_constructeur)
{
    Plateau p;
    Joueur j1("pseudo1",Couleur::Noir);
    Joueur j2("pseudo2",Couleur::Blanc);

    CHECK(j1.getPseudo() == std::string("pseudo1"));
    CHECK(j1.getCouleur() == Couleur::Noir);
    CHECK(j1.getNbPions() == Constantes::NB_PIONS_JOUEUR);

    CHECK(j2.getPseudo() == std::string("pseudo2"));
    CHECK(j2.getCouleur() == Couleur::Blanc);
    CHECK(j2.getNbPions() == Constantes::NB_PIONS_JOUEUR);

}


TEST(GroupJeu, Jeu_Exception_Plus_De_Pion)
{
    Plateau p;
    Joueur j1("pseudo1",Couleur::Noir);
    Joueur j2("pseudo2",Couleur::Blanc);
    j1.setNbPions(0);
    j2.setNbPions(0);

    CHECK(j1.getPseudo() == std::string("pseudo1"));
    CHECK(j1.getCouleur() == Couleur::Noir);
    CHECK(j1.getNbPions() == 0);

    CHECK(j2.getPseudo() == std::string("pseudo2"));
    CHECK(j2.getCouleur() == Couleur::Blanc);
    CHECK(j2.getNbPions() == 0);

    try
    {
        j1.poserPion(1,1,&p,true);
        FAIL( "exception non levee" );
    }
    catch (NoMorePawnLeft& e)
    {
        CHECK_EQUAL(e.what(), e.what());
    }
}


TEST(GroupJeu, Jeu_Exception_CoordonneesIncorrectes)
{
    Plateau p;
    Joueur j1("pseudo1",Couleur::Noir);
    Joueur j2("pseudo2",Couleur::Blanc);

    CHECK(j1.getPseudo() == std::string("pseudo1"));
    CHECK(j1.getCouleur() == Couleur::Noir);
    CHECK(j1.getNbPions() == 60);

    CHECK(j2.getPseudo() == std::string("pseudo2"));
    CHECK(j2.getCouleur() == Couleur::Blanc);
    CHECK(j2.getNbPions() == 60);

    try
    {
        j1.poserPion(9,9,&p,true);

        j2.poserPion(1,1,&p);
        FAIL( "exception non levee" );
    }
    catch (WrongPawnCoordinates& e)
    {
        CHECK_EQUAL(e.what(), e.what());
    }
}


TEST(GroupJeu, Jeu_Victoire_Diagonal_Haut_gauche_Bas_droit)
{
    Plateau p;
    Joueur j1("pseudo1",Couleur::Noir);
    Joueur j2("pseudo2",Couleur::Blanc);

    CHECK(j1.getPseudo() == std::string("pseudo1"));
    CHECK(j1.getCouleur() == Couleur::Noir);
    CHECK(j1.getNbPions() == Constantes::NB_PIONS_JOUEUR);

    CHECK(j2.getPseudo() == std::string("pseudo2"));
    CHECK(j2.getCouleur() == Couleur::Blanc);
    CHECK(j2.getNbPions() == Constantes::NB_PIONS_JOUEUR);

    j1.poserPion(18,1,&p,true);
    j2.poserPion(17,2,&p);
    j2.poserPion(16,3,&p);
    j2.poserPion(15,4,&p);
    j2.poserPion(14,5,&p);
    CHECK_EQUAL(p.checkDiagonale(14,5), false);
    j2.poserPion(13,6,&p);
    CHECK_EQUAL(p.checkDiagonale(13,6), true);

}


TEST(GroupJeu, Jeu_Victoire_Diagonal_Haut_droit_Bas_gauche)
{
    Plateau p;
    Joueur j1("pseudo1",Couleur::Noir);
    Joueur j2("pseudo2",Couleur::Blanc);

    CHECK(j1.getPseudo() == std::string("pseudo1"));
    CHECK(j1.getCouleur() == Couleur::Noir);
    CHECK(j1.getNbPions() == Constantes::NB_PIONS_JOUEUR);

    CHECK(j2.getPseudo() == std::string("pseudo2"));
    CHECK(j2.getCouleur() == Couleur::Blanc);
    CHECK(j2.getNbPions() == Constantes::NB_PIONS_JOUEUR);

    j1.poserPion(1,1,&p,true);
    j2.poserPion(2,2,&p);
    j1.poserPion(3,3,&p);
    j1.poserPion(4,4,&p);
    j1.poserPion(5,5,&p);
    j1.poserPion(6,6,&p);
    CHECK_EQUAL(p.checkDiagonale(6,6), false);
    j1.poserPion(7,7,&p);
    CHECK_EQUAL(p.checkDiagonale(7,7), true);
}





TEST(GroupJeu, Jeu_Victoire_Horizontal)
{
    Plateau p;
    Joueur j1("pseudo1",Couleur::Noir);
    Joueur j2("pseudo2",Couleur::Blanc);

    CHECK(j1.getPseudo() == std::string("pseudo1"));
    CHECK(j1.getCouleur() == Couleur::Noir);
    CHECK(j1.getNbPions() == Constantes::NB_PIONS_JOUEUR);

    CHECK(j2.getPseudo() == std::string("pseudo2"));
    CHECK(j2.getCouleur() == Couleur::Blanc);
    CHECK(j2.getNbPions() == Constantes::NB_PIONS_JOUEUR);

    j1.poserPion(3,3,&p,true);
    j2.poserPion(4,4,&p);
    j2.poserPion(4,5,&p);
    j2.poserPion(4,6,&p);
    j2.poserPion(4,7,&p);
    CHECK_EQUAL(p.checkHorizontal(), false);
    j2.poserPion(4,8,&p);
    CHECK_EQUAL(p.checkHorizontal(), true);
}


TEST(GroupJeu, Jeu_Victoire_Vertical)
{
    Plateau p;
    Joueur j1("pseudo1",Couleur::Noir);
    Joueur j2("pseudo2",Couleur::Blanc);

    CHECK(j1.getPseudo() == std::string("pseudo1"));
    CHECK(j1.getCouleur() == Couleur::Noir);
    CHECK(j1.getNbPions() == Constantes::NB_PIONS_JOUEUR);

    CHECK(j2.getPseudo() == std::string("pseudo2"));
    CHECK(j2.getCouleur() == Couleur::Blanc);
    CHECK(j2.getNbPions() == Constantes::NB_PIONS_JOUEUR);

    j1.poserPion(3,3,&p,true);
    j2.poserPion(4,4,&p);
    j2.poserPion(5,4,&p);
    j2.poserPion(6,4,&p);
    j2.poserPion(7,4,&p);
    CHECK_EQUAL(p.checkVertical(), false);
    j2.poserPion(8,4,&p);
    CHECK_EQUAL(p.checkVertical(), true);

}
