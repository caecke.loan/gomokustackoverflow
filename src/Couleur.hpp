//
// Created by Loan Caecke on 2019-04-29.
//

#ifndef PROJETCPPGL_COULEUR_HPP
#define PROJETCPPGL_COULEUR_HPP

/**
 * Représente les différents types de couleur possibles dans la partie.
 * (Blanc, Noir ou Vide)
 */
enum class Couleur {
    Blanc = 1,
    Noir = 2,
    Vide = 0
};


#endif //PROJETCPPGL_COULEUR_HPP
