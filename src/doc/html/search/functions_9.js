var searchData=
[
  ['savewinner',['saveWinner',['../class_tools.html#ab9c129bef7cd0ad3a14f713bfb4e9fda',1,'Tools']]],
  ['setcouleur',['setCouleur',['../class_joueur.html#a27e8d8b8ec766d02b5508f12bbde7754',1,'Joueur::setCouleur()'],['../class_point.html#a10d04a1ee26ee57c062967d4e2d7a87b',1,'Point::setCouleur()']]],
  ['setjoueur1',['setJoueur1',['../class_jeu.html#a66d9207108b869c34b4d4fcbed3a6123',1,'Jeu']]],
  ['setjoueur2',['setJoueur2',['../class_jeu.html#a1436d92896dbb6a36b8d9fbb6736360a',1,'Jeu']]],
  ['setnbpions',['setNbPions',['../class_joueur.html#ab003f6a4574165626ad437492b2cde6f',1,'Joueur']]],
  ['setp',['setP',['../class_jeu.html#a152c50e26a336590775f732baf5a472d',1,'Jeu']]],
  ['setpionat',['setPionAt',['../class_plateau.html#a0a4fbd18042b9d89a8e4e8e46aea0f25',1,'Plateau']]],
  ['setpseudo',['setPseudo',['../class_joueur.html#adc95a4a0a66297f0b0b90318280b8b70',1,'Joueur']]],
  ['setx',['setX',['../class_point.html#aa1d444024813ee4def15bb8576c351fc',1,'Point']]],
  ['sety',['setY',['../class_point.html#aadbaf15691dc524ea45716ef3fa24285',1,'Point']]]
];
