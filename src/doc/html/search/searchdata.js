var indexSectionsWithContent =
{
  0: "abcgjlmnoprstv~",
  1: "cjpt",
  2: "cjmpt",
  3: "acgjlmoprs~",
  4: "mn",
  5: "c",
  6: "bnv",
  7: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "related"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Énumérations",
  6: "Valeurs énumérées",
  7: "Amis"
};

