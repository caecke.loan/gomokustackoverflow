//
// Created by Loan Caecke on 2019-04-29.
//

#ifndef PROJETCPPGL_POINT_HPP
#define PROJETCPPGL_POINT_HPP


#include "Couleur.hpp"
class Point {

private:
    Couleur _couleur;
    int _x;
    int _y;

public:

    Point();
    Point(int x, int y);
    Point(int x, int y, Couleur couleur);

    void setX(int _x);
    void setY(int _y);

};


#endif //PROJETCPPGL_POINT_HPP
