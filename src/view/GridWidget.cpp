//
// Created by Loan Caecke on 2019-05-05.
//

#include "GridWidget.hpp"
#include "CaseWidget.hpp"
#include "../Jeu.hpp"
#include <Wt/WContainerWidget.h>
#include <Wt/WText.h>
#include <Wt/WBreak.h>

GridWidget::GridWidget() : Wt::WContainerWidget() {


    Jeu jeu;

    //Grid initialisation
    for (int i = 0; i < 19; i++) {
        for (int j = 0; j < 19; j++) {

            std::unique_ptr<CaseWidget> caseGrille(Wt::cpp14::make_unique<CaseWidget>(i,j,&jeu));
            addWidget(std::move(caseGrille));
        }
        addWidget(Wt::cpp14::make_unique<Wt::WBreak>());
    }

}

