//
// Created by Loan Caecke on 2019-05-05.
//

#ifndef PROJET_GOMOKU_CASEWIDGET_HPP
#define PROJET_GOMOKU_CASEWIDGET_HPP


#include <Wt/WTemplate.h>
#include <Wt/WImage.h>
#include "../Point.hpp"
#include "../Jeu.hpp"

class CaseWidget : public Wt::WImage {
private:
    Point _point;
    Jeu *_jeu;

public:
    CaseWidget(int i, int j, Jeu* jeu);
    ~CaseWidget();

    void clicHandler();
};


#endif //PROJET_GOMOKU_CASEWIDGET_HPP
