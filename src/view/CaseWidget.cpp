//
// Created by Loan Caecke on 2019-05-05.
//

#include "CaseWidget.hpp"
#include "GridWidget.hpp"
#include <Wt/WLink.h>
#include <Wt/WText.h>
#include <Wt/WImage.h>
#include <Wt/WSignal.h>

CaseWidget::CaseWidget(int i, int j, Jeu* jeu) :
    WImage("https://image.noelshack.com/fichiers/2019/19/3/1557321004-case-vide.png"),  // creating WImage of a case
    _point(i,j) {

    _jeu = jeu;
    resize(30, 30); // Resize case widget to 30x30

    std::cout << _jeu->j1 << std::endl;  // Should print 0 in the terminal [OK]

    clicked().connect(this, &CaseWidget::clicHandler);
}

void CaseWidget::clicHandler() {
    std::cout << "caseclick" << std::endl;
    std::cout << _jeu->j1 << std::endl; // Should print 0 in the terminal [KO ----> PRINTS RANDOM NUMBER]
}

CaseWidget::~CaseWidget() {
    std::cout << "- - - - - - - - CASE DELETED - - - - - - - -" << std::endl;

}
