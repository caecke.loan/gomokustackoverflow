//
// Created by Loan Caecke on 2019-05-03.
//

#ifndef PROJET_GOMOKU_APPGOMOKU_HPP
#define PROJET_GOMOKU_APPGOMOKU_HPP

#include <Wt/WApplication.h>
#include "GridWidget.hpp"

/**
 * Classe mère encapsulant l'intégralité des widgets du jeu.
 */
class AppGomoku : public Wt::WApplication {
private:
    GridWidget *_grille;

public:
    AppGomoku(const Wt::WEnvironment & env);
};




#endif //PROJET_GOMOKU_APPGOMOKU_HPP
