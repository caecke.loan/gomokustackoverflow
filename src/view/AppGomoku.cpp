//
// Created by Loan Caecke on 2019-05-03.
//

#include "AppGomoku.hpp"
#include "GridWidget.hpp"
#include <Wt/WTemplate.h>
#include <Wt/WLineEdit.h>
#include <Wt/WPushButton.h>
#include <Wt/WContainerWidget.h>


/**
 * Main of the application. Launching the server at localhost:8080 and creating an instance of AppGomoku.
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {

    return Wt::WRun(argc,argv,[](const Wt::WEnvironment& env) {
        return std::make_unique<AppGomoku>(env);
    });
}


/**
 * Entry point of the application. Creating a GridWidget.
 * @param env
 */
AppGomoku::AppGomoku(const Wt::WEnvironment & env) : Wt::WApplication(env) {
    _grille = new GridWidget();
    root()->addWidget(std::unique_ptr<Wt::WWidget>(_grille));
}
